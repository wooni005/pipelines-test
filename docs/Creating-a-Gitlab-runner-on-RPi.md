# Runner on Raspberry Pi

Info: <https://dev.to/arxeiss/gitlab-runner-on-raspberry-pi-2p7h>

### Installing the runner on the RPi

```bash
$ cat /etc/os-release
...
VERSION_CODENAME=bookworm
...

### Fill in the VERSION_CODENAME (bookworm) in the next command
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo os=debian dist=bookworm bash

$ sudo apt install gitlab-runner
```

### Create the runner in GitLab

Go in Gitlab to Settings->CI/CD->Runners->Expand:

- Click on New project runner 
  - Operating system, select: **Linux**
  - Tags: **RPi4**
  - Click on: **Create runner**
  - Store the token showed on this page
  - Enable shared runners for this project: **Off**

Remark: The tag **RPi4** will be used in the pipeline config for running the jobs on this Raspberry Pi 4.

### Register the runner on the RPi

On the RPi, register the installed runner, use the token from the previous section

```bash
$ sudo gitlab-runner register --url https://gitlab.com --token glrt-xxxxxxxxxxxxxxxxxx
```

Runtime platform                                    arch=arm64 os=linux pid=4861 revision=c72a09b6 version=16.8.0 Running in system-mode.

Enter the GitLab instance URL (for example, <https://gitlab.com/>): [<https://gitlab.com>]:  **<enter>**  
Verifying runner... is valid                        runner=s123456789  
Enter a name for the runner. This is stored only in the local config.toml file:  
[rpi-tieto]: **<enter>**  
Enter an executor: ssh, parallels, docker-windows, docker-autoscaler, instance, custom, shell, virtualbox, docker, docker+machine, kubernetes:  
**shell**  
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"

### Add user gitlab-runner to the sudoers list

```bash
$ sudo adduser gitlab-runner sudo

### Add gitlab-runner after the %sudo line
$ sudo nano /etc/sudoers
...
# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL
gitlab-runner ALL=(ALL) NOPASSWD:ALL
...
```

Commands:

```bash
sudo gitlab-runner run
sudo gitlab-runner restart
sudo gitlab-runner --help
### Show runner logging
sudo journalctl -u gitlab-runner -f
```

### Contents of the Gitlab-runner config file

```bash
$ cat /etc/gitlab-runner/config.toml
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "rpi-tieto"
  url = "https://gitlab.com"
  id = 12345678
  token = "glrt-xxxxxxxxxxxxxxxxxx"
  token_obtained_at = 2024-01-22T14:34:46Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  [runners.cache]
    MaxUploadedArchiveSize = 0
```