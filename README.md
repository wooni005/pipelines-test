# Python development template

Contains a template to start a Python project with code checks, testing, building and deploying via a Gitlab pipeline.

## Code checks and testing
The same configuration for code checking and testing is used in 3 ways:
* First in VS Code while coding
* When running tox in the development environment
* When committing changes and checks will run in the pipeline

Configuration can be found in the following files in the root directory:
* tox.ini -> For Flake8 and running tox
* .pylintrc -> For Pylint

## Tox

Install Tox with pip:

```bash
sudo pip3 install tox
```

Run tox just by calling `tox` in the project directory

```bash
tox
```

Install the extensions:
* Python (Official)
* Flake8 (Microsoft) -> Gets the configuration from tox.ini
* Pylint (Microsoft) -> Gets the configuration from .pylintrc

Extra recommended extension for VS Code:
* GitLens: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
* Better Comments: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments
* Code Runner: https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner
* autoDocstring: https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring
* Python Test Explorer: https://marketplace.visualstudio.com/items?itemName=LittleFoxTeam.vscode-python-test-adapter
* Intellicode: https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode
  * Remark: Needs Pylance and make sure that you're using Pylance as the Python language server by opening the VSCode settings page (File -> Preferences -> Settings)
* Prettier: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
* Live Share: https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare
* Code Spell Checker: https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker
* Indent Rainbow: https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow
* Todo Tree: https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree
* Black (Auto formatter): https://marketplace.visualstudio.com/items?itemName=ms-python.black-formatter
* Ruff (Python Linter better, faster than Pylint): 
* YAML: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml
* Tabnine: https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode
* Sourcery: https://marketplace.visualstudio.com/items?itemName=sourcery.sourcery


## Pylint

Install pylint with pip:

```bash
sudo pip3 install pylint
```

In the root of the directory is the config file for pylint (.pylintrc).

To run pylint on all the Python files:

```bash
pylint --rcfile=.pylintrc *
```
Remark: To run recursive over all files a subdirectory, this directory needs to have a __init__.py file

## Flake8

Install flake8 with pip:

```bash
sudo pip3 install flake8
```

To run flake8 on all the Python files:

```bash
flask8 .
```

## Pytest

Install pytest with pip:
```bash
sudo pip3 install pytest
```

To run pytest on all the Python files:
```bash
cd tests
pytest
```

## Results when running tox

```bash
$ tox
flake8: commands[0]> flake8
flake8: OK ✔ in 0.63 seconds
pylint: commands[0]> pylint --rcfile=.pylintrc miles_to_km/

-------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 8.40/10, +1.60)

pylint: OK ✔ in 2.61 seconds
pytest: commands[0]> pytest tests/
=========================================== test session starts ============================================
platform linux -- Python 3.9.2, pytest-7.4.4, pluggy-1.4.0
cachedir: .tox/pytest/.pytest_cache
rootdir: /home/pi/pipelines-showcase
collected 8 items

tests/unit/test_miles_to_km_pytest.py ....                                                           [ 50%]
tests/unit/test_miles_to_km_unittest.py ....                                                         [100%]

============================================ 8 passed in 0.07s =============================================
  flake8: OK (0.62=setup[0.13]+cmd[0.50] seconds)
  pylint: OK (2.61=setup[0.03]+cmd[2.58] seconds)
  pytest: OK (0.73=setup[0.02]+cmd[0.71] seconds)
  congratulations :) (5.03 seconds)
```

## The Gitlab pipeline
Use the pipeline to build and deploy the Docker container(s).

In this example, the runner is installed on a Raspberry Pi 4, to install and configure the runner, you can find instructions here: [docs/Creating-a-Gitlab-runner-on-RPi](docs/Creating-a-Gitlab-runner-on-RPi.md)

**Docker example**
When the container is deployed successfully, the demo webpage (file: index.html) will be shown on port 80 of the Raspberry Pi 4.
